// // Technische Keuzemodule JavaScript - Killyan Noor

// console.log("Gemaakt door Killyan Noor");


// // Values & Variables

// const country = "the Netherlands";
// const continent = "Europe";
// let population = 17;

// console.log(`I live in ${country}, which is in the continent ${continent}.

// There are currently ${population} million people living here!`);


// // Data Types

// const isIsland = false;
// let language;

// console.log(typeof isIsland, typeof population, typeof country, typeof language);


// // let, const and var
// language = "Dutch";
// // Language moet een let zijn in dit geval, omdat je dit op een later moment nog wijzigt.
// // Ik heb ook van population een let gemaakt omdat dit aantal elke dag stijgt.


// // Basic Operators
// let halfPopulated = population / 2;
// console.log(`Er zouden ${halfPopulated} miljoen inwoners zijn als het door de helft gesplitst zou worden`);

// population++;
// console.log(population);

// const populationFinland = 6;
// let moreThanFinland = population > populationFinland;
// console.log(moreThanFinland)

// const averagePopulation = 33;
// let lessThanAverage = population <  33;console.log(lessThanAverage);

// let description = `${country} is in ${continent}, and its ${population} million people speak ${language}`;
// console.log(description);


// // Taking Decisions: If / else Statements
// if(population > 33)
// {
//   console.log(`${country}'s population is greater than 33 million'`);
// } else {
//   console.log(`${country}'s population is ${(averagePopulation - population)} million below average!`);
// }

// // Type Conversion and Coercion
//   // Mijn verwachte uitkomsten
//     // '9' - '5' = 4
//     // '19' - '13' + '17' = 23
//     // '19' - '13' + 18 = 617
//     // '123' < 57 = false
//     // 5 + 6 + '4' + 9 - 4 - 2 = 1143
  
//   // De berekening
//   console.log('9' - '5'); //Goed
//   console.log('19' - '13' + '17'); //Fout
//   console.log('19' - '13' + 18); //Fout
//   console.log('123' < 57); //Goed
//   console.log(5 + 6 + '4' + 9 - 4 - 2); //Goed


// // Equality Operators: == vs ===

//   // let numNeigbours = prompt(Number("How many neighbour countries does your country have?"));
//   //  if (numNeigbours == 1) console.log("Only 1 border!");
//   //  else if (numNeigbours > 1) console.log("More than 1 border");
//   //  else console.log("No borders");
//   //   // Er moet een === gebruikt worden samen met conversion omdat er anders een string wordt gemaakt van de user input.

  
// // Logical Operators 

// if (language == "English" && population < 50 && isIsland == false)
// {
//   console.log(`You should live in ${country}!`);
// }

// // The switch Statement
// switch(language) {
//   case 'chinese':
//   case 'mandarin' :
//     console.log('MOST number of native speakers!');
//     break;
//   case 'spanish':
//       console.log('2nd place in number of native speakers');
//       break;
//     case 'english':
//       console.log('3rd place');
//       break;
//     case 'hindi':
//       console.log('Number 4');
//       break;
//     case 'arabic':
//       console.log('5th most spoken language');
//       break;
//     default:
//       console.log('Great language too :D');
// }

// // The Conditional (Ternary) Operator

// population > 33 ? console.log(`${country}'s population is above average`)
//  : console.log(`${country}'s population is below average!`);


// Functions

// function describeCountry(country, population, capitalCity){
//   return `${country} has ${population} million people and its capital city is ${capitalCity}.`;
// }

// const describeNetherlands = describeCountry("The Netherlands", 17, "Amsterdam");
// const describeGermany = describeCountry("Germany", 83, "Berlin");
// const describeFinland = describeCountry("Finland", 6, "Helsinki");
// console.log(describeNetherlands, describeGermany, describeFinland);


// Function Declaration vs. Expressions

  // Declaration
function percentageOfWorld1(population) {
  return `${(population / 7900) * 100}`;
}
const country1 = percentageOfWorld1(17)
const country2 = percentageOfWorld1(104)
const country3 = percentageOfWorld1(1441)

console.log(country1, country2, country3);

  // Expression
const percentageOfWorld2 = function (population) {
  return (population / 7900) * 100;
}
const expressionCountry1 = percentageOfWorld2(17);
const expressionCountry2 = percentageOfWorld2(104);
const expressionCountry3 = percentageOfWorld2(1441);

console.log(expressionCountry1, expressionCountry2, expressionCountry3);

// Arrow Functions
const percentageOfWorld3 = population => (population / 7900) * 100;

const arrowCountry1 = percentageOfWorld3(17);
const arrowCountry2 = percentageOfWorld3(104);
const arrowCountry3 = percentageOfWorld3(1441);
console.log(arrowCountry1, arrowCountry2, arrowCountry3);


//Functions Calling Other Functions


function describePopulation(country, population) {
  const percentage = percentageOfWorld1 (population);
  const description = `${country} has ${population} million people, which is about ${percentage}% of the world`;
  console.log(description);
}

describePopulation("The Netherlands", 17);
describePopulation("Germany", 83);
describePopulation("China", 1441);



// Coding Challenge

const calcAverage = (scoreA, scoreB, scoreC) => {
  return (scoreA + scoreB + scoreC / 3);
}

const avgDolphins = calcAverage(85, 54, 41);
const avgKoalas = calcAverage(23, 34, 27);

function checkWinner(avgDolphins, avgKoalas) {
   if (avgDolphins > (avgKoalas * 2)) {
     console.log(`Dolphins win! (${avgDolphins} vs ${avgKoalas})`);
    }
     else if (avgKoalas > (avgDolphins * 2)){
       console.log("Koalas win!");
     }
   }

   checkWinner(avgDolphins, avgKoalas);

// Introduction to Arrays

const populations = [17, 83, 1441, 104];
console.log(populations.length === 4);

const percentages = [
  percentageOfWorld1(populations[0]),
  percentageOfWorld1(populations[1]),
  percentageOfWorld1(populations[2]),
  percentageOfWorld1(populations[3])
];
console.log(percentages);


// Basic Array Operations (Methods)

const neighbours = ["Germany", "Belgium", "Luxembourg"];
neighbours.push("Utopia");
console.log(neighbours);
 neighbours.pop("Utopia");
 console.log(neighbours);
 if (!neighbours.includes("Germany")){
   console.log("Probably not a central European country");
 } else {
   console.log("The array contains Germany, removing it now, see below");
 }

 neighbours[0] = "Duitsland";
 console.log(neighbours);

 // Introduction to Objects
